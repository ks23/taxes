﻿using System;
using DAL;

namespace Tests
{
    public static class SeedData
    {
        public static void PopulateTestData(TaxesDbContext dbContext)
        {
            dbContext.Tax.Add(new DAL.Entities.TaxEntity() { Municipality = "Kaunas", Date = DateTime.Now.AddDays(-15), Result = 0.2 });
            dbContext.Tax.Add(new DAL.Entities.TaxEntity() { Municipality = "Vilnius", Date = DateTime.Now.AddDays(-10), Result = 0.4 });
            dbContext.SaveChanges();
        }
    }
}