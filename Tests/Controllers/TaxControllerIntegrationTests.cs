using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Taxes;
using Taxes.ApplicationServices.Models;
using Xunit;

namespace Tests.Controllers
{
    // Run tests with - dotnet test
    public class TaxControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public TaxControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task CanGetPlayers()
        {
            // The endpoint or route of the controller action.
            var httpResponse = await _client.GetAsync("/api/tax");

            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var taxes = JsonConvert.DeserializeObject<IEnumerable<TaxListItemViewModel>>(stringResponse);
            Assert.Contains(taxes, p => p.Municipality=="Kaunas");
            Assert.Contains(taxes, p => p.Municipality == "Vilnius");
        }


        [Fact]
        public async Task CanGetPlayerById()
        {
            // The endpoint or route of the controller action.
            var httpResponse = await _client.GetAsync("/api/tax/1");

            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var tax = JsonConvert.DeserializeObject<TaxListItemViewModel>(stringResponse);
            Assert.Equal(1, tax.Id);
            Assert.Equal("Kaunas", tax.Municipality);
        }
    }
}
