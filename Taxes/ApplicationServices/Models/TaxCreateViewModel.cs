﻿using System;
using BLL.Services.TaxService.Interfaces;

namespace Taxes.ApplicationServices.Models
{
    public class TaxCreateViewModel : ITaxCreateModel
    {
        public string Municipality { get; set; }
        public DateTime Date { get; set; }
        public double Result { get; set; }
    }
}
