﻿using System;
using BLL.Services.TaxService.Interfaces;

namespace Taxes.ApplicationServices.Models
{
    public class TaxListItemViewModel : ITaxListItemModel
    {
        public int Id { get; set; }
        public string Municipality { get; set; }
        public DateTime Date { get; set; }
        public double Result { get; set; }
    }
}
