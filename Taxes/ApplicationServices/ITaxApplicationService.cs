﻿using System.Linq;
using BLL.Services;
using BLL.Services.TaxService;
using Taxes.ApplicationServices.Models;

namespace Taxes.ApplicationServices
{
    public interface ITaxApplicationService
    {
        IQueryable<TaxListItemViewModel> GetTaxes();
        TaxListItemViewModel GetTax(int id);
        void CreateTax(TaxCreateViewModel model);
    }

    public class TaxApplicationService : ITaxApplicationService
    {
        private readonly ITaxService _taxService;

        public TaxApplicationService(ITaxService taxService)
        {
            this._taxService = taxService;
        }

        public IQueryable<TaxListItemViewModel> GetTaxes()
        {
            return _taxService.GetTaxes<TaxListItemViewModel>();
        }

        public TaxListItemViewModel GetTax(int id)
        {
            return _taxService.GetTax<TaxListItemViewModel>(id);
        }

        public void CreateTax(TaxCreateViewModel model)
        {
            _taxService.CreateTax(model);
        }
    }
}
