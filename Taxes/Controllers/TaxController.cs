﻿using System;
using System.Collections.Generic;
using DAL;
using Microsoft.AspNetCore.Mvc;
using Taxes.ApplicationServices;
using Taxes.ApplicationServices.Models;

namespace Taxes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaxController : ControllerBase
    {
        private readonly ITaxApplicationService _taxApplicationService;
        private readonly TaxesDbContext _taxesDbContext;

        public TaxController(ITaxApplicationService taxApplicationService,
            TaxesDbContext taxesDbContext)
        {
            this._taxApplicationService = taxApplicationService;
            this._taxesDbContext = taxesDbContext;
        }

        [HttpGet]
        public IEnumerable<TaxListItemViewModel> Get()
        {
            return _taxApplicationService.GetTaxes();
        }

        [HttpGet("{id}", Name = "Get")]
        public TaxListItemViewModel Get(int id)
        {
            return _taxApplicationService.GetTax(id);
        }

        [HttpPost]
        public ActionResult Post([FromBody] TaxCreateViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest();

            using (var transaction = _taxesDbContext.Database.BeginTransaction())
            {
                try
                {
                    _taxApplicationService.CreateTax(model);
                    _taxesDbContext.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    return StatusCode(500);
                }
            }

            return Ok();
        }
    }
}
