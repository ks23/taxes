﻿using System;
using System.Windows.Input;
using DAL;
using Microsoft.AspNetCore.Mvc;

namespace Taxes.Controllers
{
    public class ConventionController : ControllerBase
    {
        private readonly TaxesDbContext _taxesDbContext;

        public ConventionController(TaxesDbContext taxesDbContext)
        {
            this._taxesDbContext = taxesDbContext;
        }

        protected ActionResult SaveCommand(Func<ActionResult> saveOperations)
        {
            if (!ModelState.IsValid) return BadRequest();

            using (var transaction = _taxesDbContext.Database.BeginTransaction())
            {
                try
                {
                    saveOperations();
                    _taxesDbContext.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    return StatusCode(500);
                }
            }

            return Ok();
        }
    }
}

