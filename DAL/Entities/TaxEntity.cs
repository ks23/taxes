﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class TaxEntity : BaseEntity
    {
        public string Municipality { get; set; }

        public DateTime Date { get; set; }

        public double Result { get; set; }
        
    }
}
