﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories
{
    public class TaxRepository : Repository<TaxEntity>, ITaxRepository
    {
        public TaxRepository(TaxesDbContext dbContext) : base(dbContext)
        {
        }
    }
}
