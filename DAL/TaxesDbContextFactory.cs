﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DAL
{
    public class TaxesDbContextFactory : IDesignTimeDbContextFactory<TaxesDbContext>
    {
        public TaxesDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<TaxesDbContext>();
            return new TaxesDbContext(builder.Options);
        }
    }
}
