﻿namespace DAL.Interfaces
{
    public interface IAuditableEntity : IEntity, IAuditable
    {
    }
}
