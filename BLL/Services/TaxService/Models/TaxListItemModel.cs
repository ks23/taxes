﻿using System;
using BLL.Services.TaxService.Interfaces;

namespace BLL.Services.TaxService.Models
{
    public class TaxListItemModel : ITaxListItemModel
    {
        public int Id { get; set; }

        public string Municipality { get; set; }

        public DateTime Date { get; set; }

        public double Result { get; set; }
    }
}
