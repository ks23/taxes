﻿using System;
using BLL.Services.TaxService.Interfaces;

namespace BLL.Services.TaxService.Models
{
    public class TaxCreateModel : ITaxCreateModel
    {
        public string Municipality { get; set; }
        public DateTime Date { get; set; }
        public double Result { get; set; }
    }
}
