﻿using System;

namespace BLL.Services.TaxService.Interfaces
{
    public interface ITaxCreateModel
    {
        string Municipality { get; set; }

        DateTime Date { get; set; }

        double Result { get; set; }
    }
}
