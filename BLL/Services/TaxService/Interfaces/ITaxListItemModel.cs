﻿namespace BLL.Services.TaxService.Interfaces
{
    public interface ITaxListItemModel : ITaxCreateModel
    {
        int Id { get; set; }
    }
}
