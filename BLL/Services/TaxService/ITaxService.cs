﻿using System.Linq;
using BLL.Services.TaxService.Interfaces;
using DAL.Entities;
using DAL.Repositories.Interfaces;

namespace BLL.Services.TaxService
{
    public interface ITaxService
    {
        IQueryable<T> GetTaxes<T>() where T : ITaxListItemModel, new();
        T GetTax<T>(int id) where T : ITaxListItemModel, new();
        void CreateTax(ITaxCreateModel model);
    }

    public class TaxService : ITaxService
    {
        private readonly ITaxRepository _taxRepository;

        public TaxService(
            ITaxRepository taxRepository)
        {
            this._taxRepository = taxRepository;
        }

        public IQueryable<T> GetTaxes<T>() where T : ITaxListItemModel, new()
        {
            var data =
                from tax in _taxRepository.GetAll()
                select new T
                {
                    Id = tax.Id,
                    Date = tax.Date,
                    Municipality = tax.Municipality,
                    Result = tax.Result
                };

            return data;
        }

        public T GetTax<T>(int id) where T : ITaxListItemModel, new()
        {
            var taxEntity = _taxRepository.Get(id);

            return new T
            {
                Id = taxEntity.Id,
                Date = taxEntity.Date,
                Municipality = taxEntity.Municipality,
                Result = taxEntity.Result
            };
        }

        public void CreateTax(ITaxCreateModel model)
        {
            if (model == null) return;

            var taxEntity = new TaxEntity
            {
                Municipality = model.Municipality,
                Date = model.Date,
                Result = model.Result
            };

            _taxRepository.Add(taxEntity);
        }
    }
}
